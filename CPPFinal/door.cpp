
#include "door.h"

door::door()
{
}
door::door(const string& path)
{
	load(path);
}
door::door(const door& orig)
{
}

door::~door()
{
}

void door::print() const
{
	cout << desc << endl;
}
void door::load(const string& path)
{
	// name desc area isLocked key
	ifstream inFile(path);
	if (inFile) {
		string temp;
		std::getline(inFile, temp);
		vector<string> vect = split(temp, ",");
		name = vect[0];
		desc = vect[1];
		area = vect[2];
		isLocked = atoi(vect[3].c_str());
		key = vect[4];
	}
	else {
		cout << "Oops" << endl;
	}
	inFile.close();
}
void door::examine()
{
	print();
}
void door::open()
{
}
void door::open(vector<entity> &vect)
{
	if (name != "Wooden") {
		//        size_t index = find(vect.begin(), vect.end(), key);
		size_t index;
		for (unsigned int i = 0; i < vect.size(); i++) {
			if (vect[i].getName() == key) {
				index = i;
			}
		}
		if (index <= vect.size()) {
			isLocked = 0;
			cout << "Door unlocked! You are entering the next room. ";
		}
		else {
			cout << "You shall not pass!(without a key)";
		}
	}
	else {
		cout << "You are entering the next room." << endl;
	}
}
void door::useWith()
{
	cout << "I'm sorry i can't do that Hal" << endl;
}
string door::getDesc() {
	return desc;
}

string door::getName() {
	return name;
}