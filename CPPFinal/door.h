

#ifndef DOOR_H
#define DOOR_H

#include "../utils.h"
#include "entity.h"
#include <stdlib.h>
#include <string>
using namespace std;

class door : public entity
{
public:
	door();
	door(const string& path);
	door(const door& orig);
	virtual ~door();

	virtual void load(const string& path) override;
	virtual void print() const;
	virtual void useWith();
	virtual void examine();
	virtual void open(vector<entity> &vect);
	virtual void open();
	virtual string getDesc();
	virtual string getName();


private:
	string name;
	string desc;
	string area;
	int isLocked;
	string key;
};

#endif /* DOOR_H */