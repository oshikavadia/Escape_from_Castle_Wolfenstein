#include "key.h"
#include "../utils.h"

key::key() {
}
key::key(const string& path) {
	load(path);
}

key::key(const key& orig) {
}

key::~key() {
}

string key::getDesc() {
	return desc;
}

string key::getDoorName() const {
	return doorName;
}

string key::getName() {
	return name;
}

string key::getType() const {
	return type;
}

void key::setDesc(const string& s) {
	desc = s;

}

void key::setDoorName(const string& s) {
	doorName = s;
}

void key::setName(const string& s) {
	name = s;
}

void key::setType(const string& s) {
	type = s;

}

void key::load(const string& path) {
	//type name desc doorname

	ifstream inFile(path);
	if (inFile) {
		string temp;
		std::getline(inFile, temp);
		vector<string> vect = split(temp, ",");
		type = vect[0];
		name = vect[1];
		desc = vect[2];
		doorName = vect[3];

		inFile.close();
	}
	else {
		throw "Error reading file!!";
	}
}

void key::print() const {
	std::cout << desc << std::endl;
}