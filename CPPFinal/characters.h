#ifndef CHARACTERS_H
#define CHARACTERS_H
#include "../utils.h"
#include "entity.h"
#include "weapons.h"
#include <fstream>
#include <vector>

using namespace std;

class characters : public entity
{
public:
	characters();
	virtual ~characters();
	characters(const string& path);
	virtual void print() const;
	virtual void load(const string& path);
	void attack(characters& c);
	void equip(weapons& w);
	vector<entity> getInventory() const;
	//void removeFromInventory(const entity& s);
	virtual void useWith();
	virtual void examine();
	virtual void open();
	virtual string getDesc();
	virtual string getName();

private:
	string name;
	string desc;
	float health;
	float hitChance;
	weapons weapon;
	vector<entity> inventory;
};

#endif // CHARACTERS_H