/*
Code From Niall McGuiness from moodle
*/

#include "Entities/entity.h"
//splits a string using a delimiter and returns a vector e.g. "torch/light/walk" and "/" returns torch, light, walk
std::vector<std::string> split(std::string data, std::string delimiter)
{
	std::vector<std::string> outVector;
	std::string strElement;
	std::size_t oldPos = -1;
	std::size_t pos = data.find(delimiter, oldPos + 1);
	while (pos != std::string::npos)
	{
		strElement = data.substr(oldPos + 1, pos - oldPos - 1);
		outVector.push_back(strElement);
		oldPos = pos;
		pos = data.find(delimiter, oldPos + 1);
	}
	return outVector;
}

////////////////////////////////////////////////////

void printEntity(const entity& e) {
	e.print();
}