#ifndef ROOM_H
#define ROOM_H

#include "entity.h"
#include <vector>
class room : public entity
{
public:
	room();
	virtual ~room();
	virtual void load(const string& path) override;
	virtual void print() const;
	virtual void useWith() = 0;
	virtual void examine() = 0;
	virtual void open() = 0;
	virtual string getDesc();
	virtual string getName();

private:
	string name;
	string desc;
	//    vector<entity> vect;

};

#endif // ROOM_H
