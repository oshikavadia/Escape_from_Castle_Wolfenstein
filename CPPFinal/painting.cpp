#include "painting.h"
#include <string>
painting::painting()
{
}
painting::painting(const string& path)
{
	load(path);
}
painting::~painting()
{
}
string painting::getPainting() const
{
	return p;
}
void painting::print() const
{
	cout << p << endl;
}
void painting::load(const string& path)
{
	ifstream inFile(path);
	string temp;
	if (inFile) {
		while (!inFile.eof()) {
			string t;
			std::getline(inFile, t);
			temp += t;
			temp += "\n";
		}
		p = temp;
	}
	else {
		cout << "Oops" << endl;
	}
	inFile.close();
}
string painting::getDesc()
{
	return desc;
}
string painting::getName()
{
	return name;
}
void painting::examine()
{
	print();
}
void painting::open()
{
	cout << "You can't open a living being.Try harder next time." << endl;
}
void painting::useWith()
{
	cout << "I'm sorry Dave , I'm afraid I can't do that." << endl;
}