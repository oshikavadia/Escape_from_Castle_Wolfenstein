#ifndef PAINTING_H
#define PAINTING_H
#include "entity.h"
#include <fstream>
#include <iostream>

using namespace std;
class painting : public entity
{
public:
	painting();
	painting(const string& path);
	virtual ~painting();
	virtual void load(const string& path);
	virtual void print() const;
	string getPainting() const;
	virtual void useWith();
	virtual void examine();
	virtual void open();
	virtual string getName();
	virtual string getDesc();

private:
	string p;
	string name = "";
	string desc = "";
};
#endif // PAINTING_H