#ifndef ENTITY_H
#define ENTITY_H

#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>
using std::string;
class entity
{
public:
	entity();
	virtual ~entity();
	static int IDCount;
	static int count;
	virtual void load(const string& path) = 0;
	virtual void print() const;
	virtual void useWith() = 0;
	virtual void examine() = 0;
	virtual void open() = 0;
	virtual string getName() = 0;
	virtual string getDesc() = 0;
	int getID() const;

private:
	int ID;


};
#endif // ENTITY_H