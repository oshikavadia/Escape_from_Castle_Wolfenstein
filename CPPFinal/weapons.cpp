#include "weapons.h"

weapons::weapons()
{
	hitpoints = 0;
	desc = "No weapon Equiped";
}
weapons::weapons(const string& path)
{
	load(path);
}

weapons::~weapons()
{
}

void weapons::print() const
{
	cout << desc << endl;
}
void weapons::load(const string& path)
{
	// name desc hitpoints
	ifstream inFile(path);
	if (inFile) {
		string temp;
		std::getline(inFile, temp);
		vector<string> vect = split(temp, ",");
		name = vect[0];
		desc = vect[1];
		hitpoints = atoi(vect[2].c_str());
	}
	else {
		cout << "C++ was a mistake" << endl;
	}
	inFile.close();
}
int weapons::getHitpoints() const
{
	return hitpoints;
}
string weapons::getDesc() {
	return desc;
}

string weapons::getName() {
	return name;
}