#ifndef UTILS_H
#define UTILS_H
#include <vector>
#include <string>
#include "Entities/entity.h"
std::vector<std::string> split(std::string data, std::string delimiter);
void printEntity(const entity& e);

#endif //UTILS_H