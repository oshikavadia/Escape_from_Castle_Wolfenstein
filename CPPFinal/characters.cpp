#include "characters.h"
#include <cstdlib>
#include <ctime>

characters::characters()
{
}
characters::characters(const string& path)
{
	load(path);
}
characters::~characters()
{
}
void characters::print() const
{
	cout << desc << endl;
}

void characters::load(const string& path)
{
	ifstream inFile(path);
	if (inFile) {
		string temp;
		std::getline(inFile, temp);
		vector<string> vect = split(temp, ",");
		name = vect[0];
		desc = vect[1];
		health = atoi(vect[2].c_str());
		hitChance = atoi(vect[3].c_str());
	}
	else {
		cout << "Oops" << endl;
	}
}

void characters::attack(characters& c)
{
	float cHealth = c.health;
	int rnd = rand();
	if (rnd * hitChance) {
		c.health -= weapon.getHitpoints();
	}
}

void characters::equip(weapons& w)
{
	weapon = w;
}
void characters::examine()
{
	print();
}
void characters::open()
{
	cout << "You can't open a living being.Try harder next time." << endl;
}
void characters::useWith()
{
	cout << "I'm sorry Dave , I'm afraid I can't do that." << endl;
}
//void characters::removeFromInventory(const entity& s)
//{
//    size_t index;
//        for(int i = 0; i < inventory.size(); i++) {
//            if(inventory[i].getName() == s.getName()) {
//                index = i;
//            }
//        }
//    if(index < inventory.size()) {
//        inventory.erase(inventory.begin() + index);
//    }
//}
vector<entity> characters::getInventory() const
{
	return inventory;
}
string characters::getDesc() {
	return desc;
}

string characters::getName() {
	return name;
}