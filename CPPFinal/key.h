#ifndef KEY_H
#define KEY_H

#include "entity.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::ifstream;
using std::ofstream;
using std::vector;

class key : public entity
{
public:
	key();
	key(const string& path);
	key(const key& orig);
	virtual ~key();

	virtual void load(const string& path);

	void setName(const string& s);
	void setDoorName(const string& s);
	void setDesc(const string& s);
	void setType(const string& s);
	virtual void print() const;
	virtual string getName();
	string getType() const;
	string getDoorName() const;
	virtual string getDesc();
	virtual void useWith();
	virtual void examine();
	virtual void open();

private:
	string doorName;
	string type;
	string desc;
	string name;
};

#endif /* KEY_H */