#ifndef WEAPONS_H
#define WEAPONS_H
#include "../utils.h"
#include "entity.h"
using namespace std;

class weapons : public entity
{
public:
	weapons();
	weapons(const string& path);
	virtual ~weapons();
	virtual void load(const string& path) override;
	virtual void print() const;
	int getHitpoints() const;
	virtual void useWith();
	virtual void examine();
	virtual void open();
	virtual string getDesc();
	virtual string getName();
private:
	string name;
	string desc;
	int hitpoints;
};

#endif // WEAPONS_H